require('dotenv').config()
const express = require('express')
const admin = require('firebase-admin')
const app = express()
const morgan = require('morgan')
const cors = require('cors')
const serviceAccount = require('./firebase-admin-sdk.json')

app.use(express.json())
app.use(express.urlencoded({
  extended: false
}))
app.use(cors())

if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'))

// app.use('/api/v1', router)

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://project-from-scratch-6a4c7.firebaseio.com'
})
const topic = 'newMessage';

  let android = {
    priority: "High", //mức độ ưu tiên khi push notification
    // ttl: '360000',// hết hạn trong 1h
    data: {
      title: 'asdsad',
      content: 'asdsadsad'
    }
  }
let tokenDevice;

app.post('/subscribe', (req, res) => {
  var registrationTokens = req.body.token
  tokenDevice = registrationTokens
  admin.messaging().subscribeToTopic(registrationTokens, topic)
    .then(function(response) {
      console.log(registrationTokens)
      console.log(response)
      console.log(response.errors)
      res.status(200).json({
        status: "success",
        message: "dapat subscribe"
      })
    })
    .catch(function(error) {
      res.status(400).json({
        status: "fail",
        message: "tidak dapat subscribe"
      })
    })
})
let message = {
  android: android,
  token: tokenDevice,
  topic
   // token của thiết bị muốn push notification
}

app.post('/sendMessage', (req, res) => {
  admin.messaging().send(message)
    .then((response) => {
      res.status(200).json({
        status: 'success',
        message
      })
    })
    .catch((error) => {
      console.log(error)
      res.status(400).json({
        status: 'fail',
        message: 'tidak Dapat pesan'
      })
    })
})



app.listen(process.env.PORT || 3000, console.log('Halo'))

/*
- DB_NAME: proTra
- DB_USER: proTra-tester
- DB_PASSWORD: proTra-tester
- DB_HOST: postgres
- HEROKU_APIKEY: eee6b873-9caf-4253-8162-d35b4776828a
- HEROKU_APPNAME: ga-protra
- IMGKIT_PRIVATE_APIKEY: public_47Mo7rrovl+ovePhxuJajSiRK5A=
- IMGKIT_PUBLIC_APIKEY: private_4tfqSiCtT9cPBt63Qq4m/gP1PI8=
- IMGKIT_URL: https://ik.imagekit.io/g4nt4prim4ry/proTRa
- NODE_ENV: test
- SECRET_KEY: rahasiaUmum
*/
